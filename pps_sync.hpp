//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#pragma once

#include <stdint.h>

#include "utils/StateMachine.hpp";

#include "phase_detector.hpp"

namespace pps_sync
{
	bool __edge_flag  = false;
	float phase_error = 0.f;

	namespace states
	{
		enum
		{
			WAITING_PPS_EDGE,
			MEASURE_DELAY, 
			MEASURE, 
			WAIT_RESET,
			WAIT_PPS_FALL
		};
	}

	StateMachine machine(states::WAITING_PPS_EDGE);

	void setup()
	{
		DDRC &= (1 << 5); // pps edge
	}

	void process(const uint32_t &current_timestamp)
	{
		switch(machine.state())
		{
			case(states::WAITING_PPS_EDGE): if (PINC & (1 << 5)) machine.go_to(states::MEASURE_DELAY); break;

			case(states::MEASURE_DELAY): machine.go_to_delayed(states::MEASURE, 20);       break;
			case(states::WAIT_RESET):    machine.go_to_delayed(states::WAIT_PPS_FALL, 5);  break;

			case(states::MEASURE):
			{
				phase_error = 127.f - phase_detector::__fetch_register();
				__edge_flag = true;

				phase_detector::__up_reset();

				machine.go_to(states::WAIT_RESET);	break;
			}

			case(states::WAIT_PPS_FALL):
			{
				phase_detector::__down_reset();

				if (!(PINC & (1 << 5))) machine.go_to_delayed(states::WAITING_PPS_EDGE, 2); break;
			}

		}

		machine.process(current_timestamp);
	}

	bool new_edge()
	{
		if (__edge_flag)
		{
			__edge_flag = false;
			return true;
		}

		return false;
	}
}