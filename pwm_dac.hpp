//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#include <stddef.h>

namespace pwm_dac
{
	void setup()
	{
		TCCR1A = (1 << WGM10);   // Fast PWM 8 bits 
		TCCR1B = (1 << WGM12);

		TCCR1A |= (1 << COM1A1); // clear on compare match
		TCCR1A |= (1 << COM1B1);

		TCCR1B |= (1 << CS10);   // clk / 1

		OCR1A = 127;      // bits superiores
		OCR1B = 127;

		DDRB |= (1 << 1); // portas como saída
		DDRB |= (1 << 2);
	}

	void set_output(const float& value)
	{
		int32_t integer_value = value * 0xFFFF;

		if (integer_value > 0xFFFF) integer_value = 0xFFFF;
		if (integer_value < 0x0000) integer_value = 0x0000;

		OCR1A = ((uint16_t)integer_value & 0xFF00) >> 8;
		OCR1B = ((uint16_t)integer_value & 0x00FF);
	}

	void set_word(const uint16_t value)
	{
		OCR1A = (value & 0xFF00) >> 8;
		OCR1B = (value & 0x00FF);
	}
}