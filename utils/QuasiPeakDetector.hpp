//      ___   ____  ______        __                _       
//     / _ | / / / / __/ /__ ____/ /________  ___  (_)______
//    / __ |/ / / / _// / -_) __/ __/ __/ _ \/ _ \/ / __(_-<
//   /_/ |_/_/_/ /___/_/\__/\__/\__/_/  \___/_//_/_/\__/___/
//                                           Grégory F. Gusberti

#pragma once

class QuasiPeakDetector
{
	private:
		const float a_K;
		const float a_O;

		const float r_K;
		const float r_O;

		float y_reg;

		static constexpr float time_constant_to_alpha(const float time_constant, const float Ts)
		{
			return Ts / (Ts + time_constant);
		}

	public:
		QuasiPeakDetector(const float attack_time_constant, const float release_time_constant, const float Ts) :
			a_K{QuasiPeakDetector::time_constant_to_alpha(attack_time_constant, Ts)},  a_O{1.f - a_K},
			r_K{QuasiPeakDetector::time_constant_to_alpha(release_time_constant, Ts)}, r_O{1.f - r_K}, y_reg{0.f}
		{}

		float step(const float input)
		{
			if (input > y_reg) y_reg = y_reg * a_O + input * a_K;
			else               y_reg = y_reg * r_O + input * r_K;

			return y_reg;
		}

		float output() const
		{
			return y_reg;
		}
};