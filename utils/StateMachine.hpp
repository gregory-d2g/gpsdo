/*******************************************************************************
       ____ ___   ______   ______                      __            _
      / __ \__ \ / ____/  /_  __/__  _________  ____  / /___  ____ _(_)___ _
     / / / /_/ // / __     / / / _ \/ ___/ __ \/ __ \/ / __ \/ __ `/ / __ `/
    / /_/ / __// /_/ /    / / /  __/ /__/ / / / /_/ / / /_/ / /_/ / / /_/ /
   /_____/____/\____/    /_/  \___/\___/_/ /_/\____/_/\____/\__, /_/\__,_/
                                                           /____/
                                             Diego Von e Grégory Gusberti @ 2020
*******************************************************************************/

#pragma once

class StateMachine
{
	private:
		
		uint8_t  current_state;

		uint8_t  go_to_delayed_state;
		uint8_t  go_to_delayed_target;
		uint32_t go_to_delayed_timestamp;
		uint32_t go_to_delayed_delay;

	public:

		StateMachine(const uint8_t initial_state) :
			go_to_delayed_state(0),
			go_to_delayed_target(0),
			go_to_delayed_timestamp(0),
			go_to_delayed_delay(0)
		{
			current_state = initial_state;
		}

		void print()
		{
			Serial.print("go_to_delayed_state: "); Serial.println(go_to_delayed_state);
			Serial.print("go_to_delayed_target: "); Serial.println(go_to_delayed_target);
			Serial.print("go_to_delayed_timestamp: "); Serial.println(go_to_delayed_timestamp);
			Serial.print("go_to_delayed_delay: "); Serial.println(go_to_delayed_delay);
		}

		void go_to(uint8_t state)
		{
			current_state = state;

			go_to_delayed_state = 0;
		}

		void go_to_delayed(const uint8_t state, const uint32_t &delay)
		{
			if (go_to_delayed_state != 0) return; // o comando já foi executado 

			go_to_delayed_target = state;
			go_to_delayed_delay  = delay;

			go_to_delayed_state = 1;
		}

		uint8_t state()
		{
			return current_state;
		}

		void process(const uint32_t &current_timestamp)
		{
			if (go_to_delayed_state == 1)
			{
				go_to_delayed_timestamp = current_timestamp;

				go_to_delayed_state = 2;
			}

			if (go_to_delayed_state == 2)
			{
				if (current_timestamp - go_to_delayed_timestamp > go_to_delayed_delay)
				{
					go_to(go_to_delayed_target);
				}
			}
		}
};